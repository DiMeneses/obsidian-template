### Gitlab pages template
Place your markdown files in `docs/`.
Edit `mkdocs.yml` config to suit you.
Push to gitlab to launch your pages.
Find [this link](`https://dimeneses.gitlab.io/obsidian-template/`) under `Settings->Pages`.